import flask
from flask_table import Table, Col
import re
from flask import url_for
from operator import itemgetter

app = flask.Flask(__name__)

@app.route("/")
def main_route():
    f = open("/var/log/suricata/fast.log", "r")
    s = f.readlines()[::-1]
    f.close()
    elems = [parse_log_line(line) for line in s]
    
    print(elems)

    new_elems = []
    for elem in elems:
        new_elems.append(LogEntry(*elem))
    

    table = LogTable(new_elems)
    f = open("style.html", "r")
    s = f.read()
    f.close()
    return(s +"\n"+ table.__html__())

class LogTable(Table):
    date = Col("Date")
    protocol = Col("Protocol")
    from_col = Col("From")
    to_col = Col("To")
    name = Col("Rule name")

class LogEntry():
    def __init__(self, date, protocol, from_col, to_col, name):
        self.date = date
        self.protocol = protocol
        self.from_col = from_col
        self.to_col = to_col
        self.name = name


def parse_log_line(line):
    protocol_re = re.compile("\{([A-Za-z]+)\}")
    ip_re = re.compile("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\:(\d){1,5}")
    date = (lambda x: x[1] + "-" + x[0])(line.split()[0].split("-"))
    protocol = protocol_re.search(line).group(1)
    rule_name = line.split()[3]
    from_ip, to_ip = line.split()[-1], line.split()[-3]
    return date, protocol, to_ip, from_ip, rule_name

if __name__ == "__main__":
    app.run(host="0.0.0.0")
